// Fill out your copyright notice in the Description page of Project Settings.

#include "TLOUExposureMapSubsystem.h"

#include "AI/Navigation/NavigationTypes.h"
#include "GameFramework/Character.h"
#include "Kismet/GameplayStatics.h"
#include "NavigationSystem.h"

void UTLOUExposureMapSubsystem::OnWorldBeginPlay(UWorld& InWorld)
{
	Super::OnWorldBeginPlay(InWorld);
	UpdateGridData();
}

void UTLOUExposureMapSubsystem::Tick(float DeltaTime)
{
	TRACE_CPUPROFILER_EVENT_SCOPE(UTLOUExposureMapSubsystem::Tick);

	UTickableWorldSubsystem::Tick(DeltaTime);

	if (PreviousComputeMode != ComputeMode)
	{
		UE::Tasks::Wait(RaycastTasks);
		RaycastTasks.Reset();
		TimeSliceFrameIndex = 0;
		PreviousComputeMode = ComputeMode;
	}

	ACharacter* PlayerCharacter = UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
	if (!PlayerCharacter)
	{
		return;
	}

	FVector EyesLocation;
	FRotator HeadRotation;
	PlayerCharacter->GetActorEyesViewPoint(EyesLocation, HeadRotation);

	switch (ComputeMode)
	{
		case ETLOUExposureMapComputeMode::SingleThreaded:
			UpdateExposure_SingleThreaded(EyesLocation);
			break;

		case ETLOUExposureMapComputeMode::MultiThreadedGatherSameFrame:
			UpdateExposure_MultiThreadedGatherSameFrame(EyesLocation);
			break;

		case ETLOUExposureMapComputeMode::MultiThreadedGatherNextFrame:
			UpdateExposure_MultiThreadedGatherNextFrame(EyesLocation);
			break;

		case ETLOUExposureMapComputeMode::MultiThreadedTimeSlicing:
			UpdateExposure_MultiThreadedTimeSlicing(EyesLocation);
			break;

		default:
			checkNoEntry();
			break;
	}

	UpdatePathLength(EyesLocation);

	DebugDrawExposureMap();
}

TStatId UTLOUExposureMapSubsystem::GetStatId() const
{
	RETURN_QUICK_DECLARE_CYCLE_STAT(UTLOUExposureMapSubsystem, STATGROUP_Tickables);
}

void UTLOUExposureMapSubsystem::SetExposureMapComputeMode(ETLOUExposureMapComputeMode Mode)
{
	ComputeMode = Mode;
}

bool UTLOUExposureMapSubsystem::IsExposed(const FVector& Position) const
{
	const uint16 CellIndex = PositionToCellIndex(Position);
	if (CellIndex > ExposureMap.Num())
	{
		return false;
	}

	return ExposureMap[CellIndex];
}

bool UTLOUExposureMapSubsystem::FindNearestNonExposedPosition(const FVector& CurrentPosition, FVector& PositionOut) const
{
	float BestPathLen = TNumericLimits<float>::Max();
	bool bFound = false;
	uint16 BestCellIndex = 0;

	const uint16 CellCount = GetCellCount();
	for (uint16 CellIndex = 0; CellIndex < CellCount; ++CellIndex)
	{
		if (!ClearanceMap[CellIndex])
		{
			continue;
		}

		if (ExposureMap[CellIndex])
		{
			continue;
		}

		const float PathLen = PathLengthMap[CellIndex];
		if (PathLen >= BestPathLen)
		{
			continue;
		}

		BestPathLen = PathLen;
		BestCellIndex = CellIndex;
		bFound = true;
	}

	PositionOut = Grid[BestCellIndex];
	return bFound;
}

void UTLOUExposureMapSubsystem::UpdateGridData()
{
	const uint16 NumCells = GetCellCount();
	Grid.SetNum(NumCells);
	PathLengthMap.SetNum(NumCells);
	ClearanceMap.SetNum(NumCells);
	ExposureMap.SetNum(NumCells);

	RaycastTasks.Reserve(GetTaskBatchSize());

	for (uint8 Row = 0; Row < GridDimension; ++Row)
	{
		for (uint8 Col = 0; Col < GridDimension; ++Col)
		{
			const uint16 CellIndex = GetCellIndex(Row, Col);
			const FVector Center = GetCellCenter(Row, Col);

			FHitResult Hit;
			FVector TraceStart = Center + 300.0f * FVector::UpVector;
			FVector TraceEnd = Center + 1.f * FVector::UpVector;	// Above ground

			// #GH_TODO add a query filter to ignore ground so that no need to hack traceend with 1.f
			const bool bHit = GetWorld()->LineTraceSingleByChannel(Hit, TraceStart, TraceEnd, ECC_Visibility);

			// DrawDebugLine(GetWorld(), TraceStart, TraceEnd, FColor(0, 255, 0), true);

			Grid[CellIndex] = Center;
			PathLengthMap[CellIndex] = TNumericLimits<float>::Max();
			ClearanceMap[CellIndex] = !bHit;	// If hit means there is a platform so non-navigable
			ExposureMap[CellIndex] = false;
		}
	}
}

void UTLOUExposureMapSubsystem::UpdatePathLength(const FVector& Origin)
{
	TRACE_CPUPROFILER_EVENT_SCOPE(UTLOUExposureMapSubsystem::UpdatePathLength);

	UNavigationSystemV1* NavSystem = UNavigationSystemV1::GetCurrent(GetWorld());
	if (!NavSystem)
	{
		UE_LOG(LogScript, Error, TEXT("Failed to access UNavigationSystem in ExposureMapSubsystem. Cannot compute path lengths."));
		return;
	}

	const uint16 NumCells = GetCellCount();
	for (uint16 CellIndex = 0; CellIndex < NumCells; ++CellIndex)
	{
		if (!ClearanceMap[CellIndex])
		{
			continue;
		}

		FNavLocation OriginNav;
		if (!NavSystem->ProjectPointToNavigation(Origin, OriginNav))
		{
			continue;
		}

		FNavLocation DestinationNav;
		if (!NavSystem->ProjectPointToNavigation(Grid[CellIndex], DestinationNav))
		{
			continue;
		}

		double Length = TNumericLimits<double>::Max();
		const ENavigationQueryResult::Type Result = NavSystem->GetPathLength(OriginNav, DestinationNav, Length);
		if (Result != ENavigationQueryResult::Success)
		{
			continue;
		}

		PathLengthMap[CellIndex] = Length;
	}
}

void UTLOUExposureMapSubsystem::UpdateExposure_SingleThreaded(const FVector& EyePosition)
{
	TRACE_CPUPROFILER_EVENT_SCOPE(UTLOUExposureMapSubsystem::UpdateExposure_SingleThreaded);

	const uint16 NumCells = GetCellCount();
	for (uint16 CellIndex = 0; CellIndex < NumCells; ++CellIndex)
	{
		ExposureMap[CellIndex] = FindIfCellIsExposed(EyePosition, CellIndex);
	}
}

void UTLOUExposureMapSubsystem::UpdateExposure_MultiThreadedGatherSameFrame(const FVector& EyePosition)
{
	TRACE_CPUPROFILER_EVENT_SCOPE(UTLOUExposureMapSubsystem::UpdateExposure_MultiThreadedGatherSameFrame);

	RaycastTasks.Reset();

	const uint16 TaskSize = GetTaskBatchSize();
	const uint16 NumCells = GetCellCount();
	for (uint16 CellIndex = 0; CellIndex < NumCells; CellIndex += TaskSize)
	{
		auto JobHandle = UE::Tasks::Launch(UE_SOURCE_LOCATION,
			[this, TaskSize, CellIndex, &EyePosition]()
			{
				int SafeTaskSize = TaskSize;
				if (CellIndex + TaskSize >= ExposureMap.Num())
				{
					SafeTaskSize = ExposureMap.Num() - CellIndex;
				}

				for (int i = 0; i < SafeTaskSize; ++i)
				{
					ExposureMap[CellIndex + i] = FindIfCellIsExposed(EyePosition, CellIndex + i);
				}
			});

		RaycastTasks.Add(JobHandle);
	}

	UE::Tasks::Wait(RaycastTasks);
}

void UTLOUExposureMapSubsystem::UpdateExposure_MultiThreadedGatherNextFrame(const FVector& EyePosition)
{
	TRACE_CPUPROFILER_EVENT_SCOPE(UTLOUExposureMapSubsystem::UpdateExposure_MultiThreadedGatherNextFrame);

	// Wait for previous frame raycasts (should be done by this point so likely won't wait at all)
	UE::Tasks::Wait(RaycastTasks);

	RaycastTasks.Reset();

	const uint16 TaskSize = GetTaskBatchSize();
	const uint16 NumCells = GetCellCount();
	for (uint16 CellIndex = 0; CellIndex < NumCells; CellIndex += TaskSize)
	{
		auto JobHandle = UE::Tasks::Launch(UE_SOURCE_LOCATION,
			[this, TaskSize, CellIndex, &EyePosition]()
			{
				int SafeTaskSize = TaskSize;
				if (CellIndex + TaskSize >= ExposureMap.Num())
				{
					SafeTaskSize = ExposureMap.Num() - CellIndex;
				}

				for (int i = 0; i < SafeTaskSize; ++i)
				{
					ExposureMap[CellIndex + i] = FindIfCellIsExposed(EyePosition, CellIndex + i);
				}
			});

		RaycastTasks.Add(JobHandle);
	}
}

void UTLOUExposureMapSubsystem::UpdateExposure_MultiThreadedTimeSlicing(const FVector& EyePosition)
{
	TRACE_CPUPROFILER_EVENT_SCOPE(UTLOUExposureMapSubsystem::UpdateExposure_MultiThreadedTimeSlicing);

	if (TimeSliceFrameIndex == 0)
	{
		// Wait for previous frame(s) raycasts before starting a new batch of work
		// To prevent too large delay, could add a timeout for the wait and skip the update for this frame if too long
		UE::Tasks::Wait(RaycastTasks);
		RaycastTasks.Reset();
	}

	const uint16 TaskSize = GetTaskBatchSize();
	const uint16 NumCells = GetCellCount();

	const uint16 NumFrames = 10;	// One full refresh will take 10 frames to be complete
	const uint16 NumCellsToHandlePerFrame = NumCells / NumFrames;
	const uint16 MaxCellIndexToHandleThisFrame = FMath::Min<uint16>((TimeSliceFrameIndex + 1) * NumCellsToHandlePerFrame, NumCells);

	for (uint16 CellIndex = TimeSliceFrameIndex * NumCellsToHandlePerFrame; CellIndex < MaxCellIndexToHandleThisFrame;
		 CellIndex += TaskSize)
	{
		auto JobHandle = UE::Tasks::Launch(UE_SOURCE_LOCATION,
			[this, TaskSize, CellIndex, &EyePosition]()
			{
				int SafeTaskSize = TaskSize;
				if (CellIndex + TaskSize >= ExposureMap.Num())
				{
					SafeTaskSize = ExposureMap.Num() - CellIndex;
				}

				for (int i = 0; i < SafeTaskSize; ++i)
				{
					ExposureMap[CellIndex + i] = FindIfCellIsExposed(EyePosition, CellIndex + i);
				}
			});

		RaycastTasks.Add(JobHandle);
	}

	TimeSliceFrameIndex++;
	if (TimeSliceFrameIndex >= NumFrames)
	{
		TimeSliceFrameIndex = 0;
	}
}

void UTLOUExposureMapSubsystem::DebugDrawExposureMap() const
{
	const uint16 NumCells = GetCellCount();
	for (uint16 CellIndex = 0; CellIndex < NumCells; ++CellIndex)
	{
		if (!ClearanceMap[CellIndex])
		{
			continue;
		}

		const FPlane Plane(0.f, 0, 1.f, 1.f);
		const FVector CellCenter = Grid[CellIndex];
		const bool bExposed = ExposureMap[CellIndex];
		const FColor Color = bExposed ? FColor(255, 0, 0, 30) : FColor(0, 255, 0, 30);

		DrawDebugSolidPlane(GetWorld(), Plane, CellCenter, CellSize / 3.f, Color);
	}
}

bool UTLOUExposureMapSubsystem::FindIfCellIsExposed(const FVector& EyePosition, uint16 CellIndex) const
{
	if (!ClearanceMap[CellIndex])
	{
		return false;
	}

	const FVector CellCenter = Grid[CellIndex];
	const FVector CellToEye = CellCenter - EyePosition;
	const FVector Direction = CellToEye.GetSafeNormal();
	// #GH_TODO remove the "- 10.f" and add raycast query filter to ignore ground instead
	const float Length = CellToEye.Length() - 10.f;
	const FVector TraceEnd = EyePosition + Direction * Length;

	// DrawDebugLine(GetWorld(), EyePosition, TraceEnd, FColor(0, 255, 0));

	FHitResult Hit;
	const bool bHit = GetWorld()->LineTraceSingleByChannel(Hit, EyePosition, TraceEnd, ECC_Visibility);
	return !bHit;
}

uint16 UTLOUExposureMapSubsystem::GetCellCount() const
{
	return GridDimension * GridDimension;
}

uint16 UTLOUExposureMapSubsystem::GetCellIndex(uint8 Row, uint8 Column) const
{
	return Row * GridDimension + Column;
}

FVector UTLOUExposureMapSubsystem::GetCellCenter(uint8 Row, uint8 Column) const
{
	return FVector((Column + 0.5) * CellSize, (Row + 0.5) * CellSize, 0.);
}

uint16 UTLOUExposureMapSubsystem::PositionToCellIndex(const FVector& Pos) const
{
	const double Row = FMath::Floor(Pos.Y / CellSize);
	const double Col = FMath::Floor(Pos.X / CellSize);

	if (Row > TNumericLimits<uint8>::Max() || Col > TNumericLimits<uint8>::Max())
	{
		UE_LOG(LogScript, Warning, TEXT("%s"),
			*FString::Printf(TEXT("pos x:%s y:%s is outside of supported exposure map grid range"), Pos.X, Pos.Y));
		return 0;
	}

	return GetCellIndex(Row, Col);
}

uint16 UTLOUExposureMapSubsystem::GetTaskBatchSize() const
{
	return FMath::CeilToInt((float) GetCellCount() / FPlatformMisc::NumberOfCores());
}
