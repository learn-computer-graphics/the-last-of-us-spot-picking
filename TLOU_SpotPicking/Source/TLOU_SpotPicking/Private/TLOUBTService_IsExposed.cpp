// Fill out your copyright notice in the Description page of Project Settings.

#include "TLOUBTService_IsExposed.h"

#include "AIController.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "TLOUExposureMapSubsystem.h"

UTLOUBTService_IsExposed::UTLOUBTService_IsExposed(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	INIT_SERVICE_NODE_NOTIFY_FLAGS();

	// accept only bool
	BlackboardKey.AddBoolFilter(this, GET_MEMBER_NAME_CHECKED(UTLOUBTService_IsExposed, BlackboardKey));
}

void UTLOUBTService_IsExposed::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	if (UBlackboardComponent* Blackboard = OwnerComp.GetBlackboardComponent())
	{
		if (UTLOUExposureMapSubsystem* ExposureMap = GetWorld()->GetSubsystem<UTLOUExposureMapSubsystem>())
		{
			const bool bExposed = ExposureMap->IsExposed(OwnerComp.GetAIOwner()->GetPawn()->GetActorLocation());
			Blackboard->SetValueAsBool(BlackboardKey.SelectedKeyName, bExposed);
		}
	}

	Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);
}
