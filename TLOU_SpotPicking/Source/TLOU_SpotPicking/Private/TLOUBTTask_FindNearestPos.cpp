// Fill out your copyright notice in the Description page of Project Settings.

#include "TLOUBTTask_FindNearestPos.h"

#include "AIController.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "TLOUExposureMapSubsystem.h"

UTLOUBTTask_FindNearestPos::UTLOUBTTask_FindNearestPos(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	// accept only vectors
	BlackboardKey.AddVectorFilter(this, GET_MEMBER_NAME_CHECKED(UTLOUBTTask_FindNearestPos, BlackboardKey));
}

EBTNodeResult::Type UTLOUBTTask_FindNearestPos::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	UBlackboardComponent* Blackboard = OwnerComp.GetBlackboardComponent();
	UTLOUExposureMapSubsystem* ExposureMap = GetWorld()->GetSubsystem<UTLOUExposureMapSubsystem>();
	if (!Blackboard || !ExposureMap)
	{
		return EBTNodeResult::Failed;
	}

	FVector NearestValidPos;
	const bool bResult =
		ExposureMap->FindNearestNonExposedPosition(OwnerComp.GetAIOwner()->GetPawn()->GetActorLocation(), NearestValidPos);
	if (!bResult)
	{
		return EBTNodeResult::Failed;
	}

	Blackboard->SetValueAsVector(BlackboardKey.SelectedKeyName, NearestValidPos);
	return EBTNodeResult::Succeeded;
}
