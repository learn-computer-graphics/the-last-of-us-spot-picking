// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "BehaviorTree/Services/BTService_BlackboardBase.h"
#include "CoreMinimal.h"

#include "TLOUBTService_IsExposed.generated.h"

/**
 *
 */
UCLASS()
class TLOU_SPOTPICKING_API UTLOUBTService_IsExposed : public UBTService_BlackboardBase
{
	GENERATED_BODY()

protected:
	UTLOUBTService_IsExposed(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

	virtual void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;
};
