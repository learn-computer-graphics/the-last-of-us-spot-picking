// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "BehaviorTree/Tasks/BTTask_BlackboardBase.h"
#include "CoreMinimal.h"

#include "TLOUBTTask_FindNearestPos.generated.h"

/**
 *
 */
UCLASS()
class TLOU_SPOTPICKING_API UTLOUBTTask_FindNearestPos : public UBTTask_BlackboardBase
{
	GENERATED_BODY()

public:
	UTLOUBTTask_FindNearestPos(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

private:
	// Begin UBTTaskNode
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
	// End UBTTaskNode
};
