// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"

#include "TLOUInfectedCharacter.generated.h"

UCLASS()
class TLOU_SPOTPICKING_API ATLOUInfectedCharacter : public ACharacter
{
	GENERATED_BODY()
};
