// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"

#include "TLOUGameMode.generated.h"

/**
 *
 */
UCLASS()
class TLOU_SPOTPICKING_API ATLOUGameMode : public AGameModeBase
{
	GENERATED_BODY()
};
