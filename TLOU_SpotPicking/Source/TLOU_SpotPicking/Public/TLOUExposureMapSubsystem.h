// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Subsystems/WorldSubsystem.h"
#include "Tasks/Task.h"

#include <atomic>

#include "TLOUExposureMapSubsystem.generated.h"

UENUM(BlueprintType)
enum class ETLOUExposureMapComputeMode : uint8
{
	SingleThreaded,
	MultiThreadedGatherSameFrame,
	MultiThreadedGatherNextFrame,
	MultiThreadedTimeSlicing	// Only do a portion of the work each frame
};

/**
 * Split the map into cells of a grid. Each cell has line of sight / isExposed value and path cost between player and enemy
 * note : Could be done with the Environment Query System and AI Perception, but the purpose of this class is to test out
 * performances via multiple setups
 */
UCLASS()
class TLOU_SPOTPICKING_API UTLOUExposureMapSubsystem : public UTickableWorldSubsystem
{
	GENERATED_BODY()

public:
	// Begin UWorldSubsystem
	virtual void OnWorldBeginPlay(UWorld& InWorld) override;
	// End UWorldSubsystem

	// Begin FTickableGameObject
	virtual void Tick(float DeltaTime) override;
	virtual TStatId GetStatId() const override;
	// End FTickableGameObject

	UFUNCTION(BlueprintCallable, Category = "ExposureMap")
	void SetExposureMapComputeMode(ETLOUExposureMapComputeMode Mode);

	bool IsExposed(const FVector& Position) const;
	bool FindNearestNonExposedPosition(const FVector& CurrentPosition, FVector& PositionOut) const;

private:
	void UpdateGridData();
	void UpdatePathLength(const FVector& Origin);

	void UpdateExposure_SingleThreaded(const FVector& EyePosition);
	void UpdateExposure_MultiThreadedGatherSameFrame(const FVector& EyePosition);
	void UpdateExposure_MultiThreadedGatherNextFrame(const FVector& EyePosition);
	void UpdateExposure_MultiThreadedTimeSlicing(const FVector& EyePosition);

	void DebugDrawExposureMap() const;

	// Launch a raycast from Eye to Cell to check if something blocks
	bool FindIfCellIsExposed(const FVector& EyePosition, uint16 CellIndex) const;

	uint16 GetCellCount() const;
	uint16 GetCellIndex(uint8 Row, uint8 Column) const;
	FVector GetCellCenter(uint8 Row, uint8 Column) const;
	uint16 PositionToCellIndex(const FVector& Pos) const;

	// The number of raycast per task
	uint16 GetTaskBatchSize() const;

	ETLOUExposureMapComputeMode ComputeMode = ETLOUExposureMapComputeMode::SingleThreaded;
	ETLOUExposureMapComputeMode PreviousComputeMode = ETLOUExposureMapComputeMode::SingleThreaded;
	uint8 CellSize = 100;
	uint8 GridDimension = 40;
	TArray<FVector> Grid;
	TArray<float> PathLengthMap;	// Length from the player position at this point
	TArray<bool> ClearanceMap;		// True if grid is navigable at this point. Does not rely on the navmesh but on a local raycast
	TArray<std::atomic<bool>> ExposureMap;	  // True if grid is visible by the player at this point.
	TArray<UE::Tasks::FTask> RaycastTasks;
	uint16 TimeSliceFrameIndex = 0;
};
